export default class ArrayHelpers {
    public static moveElement(elements: any[], oldIndex: number, newIndex: number): any[] {
        const element = elements[oldIndex];
        elements.splice(oldIndex, 1);
        elements.splice(newIndex, 0, element);

        return elements;
    }

    public static deleteElementByIndex(elements: any[], index: number): any[] {
        elements.splice(index, 1);

        return elements;
    }
}
